# CSS Galery

Virtual galery using HTML, CSS and JavaScript.

This project is intended for a website where you can upload files of drawings and paintings you made about anime/manga series.

You can write a specific description of your painting, where you explain which is happening in the painting, what was your inspiration of drawing it or even the softwares that you use to make it.

Below the image of the painting you can see the anime/manga the painting is referencing, when it was published and who is the author of it. If you're a visitor, you can like the painting and even commenting your thoughts about it.

Every author have a custom profile, where all the paintings they have made will appear, along with their social media and a breve description.

In the header of the principal site you can see all the paintings sorted by most-viewed, most-liked and a-z. There's also a search bar where you can search a painting by its name, an author or an anime/manga; the page will show you all the paintings related to that anime/manga.

It's fully responsive, for computer, tablet and phone.
